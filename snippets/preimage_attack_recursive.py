import binascii as bascii
import hashlib as hslib
import random as rand
import array as arr
import copy as cpy
import time

asciiAlphabet = arr.array('u',
                          ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
                           'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                           'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'])


class Source:

    def __init__(self, alphabet, c_len, tries_for_len):
        self.a = alphabet
        self.aLen = len(self.a)
        self.c_len = c_len
        self.tries_for_len = tries_for_len

    def next(self):
        cp = cpy.copy(self.a)
        self.tries_for_len += 1
        if self.tries_for_len > self.max_hashes_for_len():
            self.c_len += 1
            self.tries_for_len = 0
            print(f'max hashes for aLen={self.aLen}, cLen={self.c_len}: {self.max_hashes_for_len()}')
        return bytes(''.join(rand.sample(cp, self.c_len)), 'ascii')

    # Calculates max possible hashes for a given length and alphabet.
    # To simplify code, no caching was implemented.
    def max_hashes_for_len(self):
        return self.aLen ** self.c_len


def preimage(hash_algo, h, source):
    s = source.next()
    print(s)
    if h == hash_algo(s):
        return s
    else:
        return preimage(hash_algo, h, source)


def print_result(hash_algo, time_passed, pre_image):
    print(f'{hash_algo} took: {time_passed}ms to find preimage "{pre_image}"')


def run_algo(a, c, s):
    try:
        return preimage(a, c, s)
    except RecursionError:
        return run_algo(a, c, s)


if __name__ == '__main__':

    algos = [bascii.crc32, hslib.sha256]
    collision = b"Satoshi Nakamoto"
    src = Source(asciiAlphabet, 20, 0)

    for algo in algos:
        print(f'RUNNING {algo}')
        exec_start = time.time()
        prei = run_algo(algo, collision, src)
        exec_end = time.time()
        print_result(algo, exec_end - exec_start, prei)
