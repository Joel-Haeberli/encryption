import binascii as bascii
import hashlib as hslib
import random as rand
import time

# to create a log output each x-th try.
log_sensitivity = 10_000_000


class Source:

    hashes = list()

    def __init__(self, c_len, tries_for_len):
        self.c_len = c_len
        self.tries_for_len = tries_for_len

    def next(self):
        self.tries_for_len += 1
        if self.tries_for_len > self.max_hashes_for_len():
            self.c_len += 1
            self.tries_for_len = 0
            print(f'max hashes for cLen={self.c_len}: {self.max_hashes_for_len()}')
        else:
            if self.tries_for_len % log_sensitivity == 0:
                print(f'tries={self.tries_for_len}')

        new_collision = rand.randbytes(self.c_len)
        while self.hashes.__contains__(new_collision):
            new_collision = rand.randbytes(self.c_len)
        self.hashes.append(new_collision)
        return new_collision

    # Calculates max possible hashes for a given length (assuming char takes 1 byte -> 1 sign = 1 byte).
    # To simplify code, no caching was implemented (duplicates possible).
    def max_hashes_for_len(self):
        return (2 ** 8) ** self.c_len


def preimage(hash_algo, hsh, source):
    s = source.next()
    h = hash_algo(s)
    while hsh != h:
        s = source.next()
        h = hash_algo(s)
    return s


def print_result(hash_algo, time_passed, pre_image):
    print(f'PREIMAGE FOUND! => {hash_algo} took: {time_passed}ms to find preimage "{pre_image}"')


if __name__ == '__main__':

    algos = [bascii.crc32, hslib.sha256]
    collision_string = "Hello World"
    src = Source(len(collision_string), 0)

    for algo in algos:
        print(f'RUNNING {algo}')
        collision_bytes = algo(bytes(collision_string, 'ascii'))
        exec_start = time.time()
        prei = preimage(algo, collision_bytes, src)
        exec_end = time.time()
        print_result(algo, exec_end - exec_start, prei)
