package ch.bfh.akka.pwdcrack.config

import akka.actor.typed.ActorRef
import ch.bfh.akka.pwdcrack.actors.RootCracker

data class UserConfiguration(
    val root: ActorRef<RootCracker.RootCrackerMessage>,
    val user: User,
    val passwords: List<String>,
    val crackers: Int = 10
)