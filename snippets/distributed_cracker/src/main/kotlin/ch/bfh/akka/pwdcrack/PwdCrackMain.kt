/*
 * Main program of Pass-Crack exercise
 */

package ch.bfh.akka.pwdcrack

import akka.actor.typed.ActorSystem
import ch.bfh.akka.pwdcrack.actors.RootCracker
import ch.bfh.akka.pwdcrack.config.RootCrackConfiguration

object PwdCrackMain {

	 @JvmStatic
	fun main(args: Array<String>) {
		 println("PwdCrack Exercise")

		 System.setProperty("akka.log-dead-letters", "false")
		 System.setProperty("akka.log-dead-letters-during-shutdown", "false")

		 val config = RootCrackConfiguration()
		 val crackSystem = ActorSystem.create(RootCracker.create(config), "Cracker-System")

		 crackSystem.tell(RootCracker.StartRootCracker())
	}
}
