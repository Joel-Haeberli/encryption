package ch.bfh.akka.pwdcrack.config

import ch.bfh.akka.pwdcrack.parser.LeakParser
import ch.bfh.akka.pwdcrack.parser.UserParser

data class RootCrackConfiguration(
    val numberOfCrackersPerUser: Int = 10,
    val users: List<User> = UserParser.parseUsers(),
    val passwords: List<String> = LeakParser.parseLeaks()
)