package ch.bfh.akka.pwdcrack.config

import akka.actor.typed.ActorRef
import ch.bfh.akka.pwdcrack.actors.UserCracker

data class CrackConfiguration(
    val parent: ActorRef<UserCracker.UserCrackerMessage>,
    val expected: String,
    val passwords: List<String>,
    val algorithm: String = "SHA-512"
)