package ch.bfh.akka.pwdcrack.actors

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.Signal
import akka.actor.typed.javadsl.AbstractBehavior
import akka.actor.typed.javadsl.ActorContext
import akka.actor.typed.javadsl.Behaviors
import akka.actor.typed.javadsl.Receive
import ch.bfh.akka.pwdcrack.config.CrackConfiguration
import ch.bfh.akka.pwdcrack.config.UserConfiguration

class UserCracker private constructor(

    ctx: ActorContext<UserCrackerMessage>,
    private val config: UserConfiguration

) : AbstractBehavior<UserCracker.UserCrackerMessage>(ctx) {

    companion object {

        fun create(config: UserConfiguration) : Behavior<UserCrackerMessage> = Behaviors.setup { ctx -> UserCracker(ctx, config) }
    }

    interface UserCrackerMessage
    class StartUserCracker : UserCrackerMessage
    class AllPasswordsTravelled : UserCrackerMessage
    data class NoMatchMessage(val cracker: ActorRef<Cracker.CrackerMessage>) : UserCrackerMessage
    data class MatchMessage(val pw: String) : UserCrackerMessage

    private var currentPasswordIndex = 0
    private var matchFound = false
    private var match = ""
    private val crackers = mutableListOf<ActorRef<Cracker.CrackerMessage>>()

    override fun createReceive() = UserCrackerReceiver(this::setupUserCracker, this::match, this::noMatch, this::finish)

    /**
     * Sets up the through the config specified number of Password-Crackers
     */
    private fun setupUserCracker() : UserCracker {

        println("Setting up crackers for ${config.user.name}")
        for (i in (0 until config.crackers)) {
            val cfg = CrackConfiguration(context.self, config.user.hash, config.passwords)
            crackers.add(context.spawn(Cracker.create(cfg), "Cracker-${config.user.name}-$i"))
        }
        crackers.forEach { context.self.tell(NoMatchMessage(it)) } // this kicks of the chain
        println("Finished setting up Cracker for user ${config.user.name} (count: ${crackers.size})")
        return this
    }

    private fun noMatch(msg: NoMatchMessage) : UserCracker {
        msg.cracker.tell(Cracker.NextPasswordMessage(config.user.salt, currentPasswordIndex))
        currentPasswordIndex += 1
        return this
    }

    private fun match(msg: MatchMessage) : Behavior<UserCrackerMessage> {
        matchFound = true
        match = msg.pw
        return finish()
    }

    private fun finish() : Behavior<UserCrackerMessage> {
        crackers.forEach { context.stop(it) }
        config.root.tell(RootCracker.Result(matchFound, config.user, match))
        return Behaviors.stopped()
    }

    class UserCrackerReceiver(
        private val setupUserCracker: () -> Behavior<UserCrackerMessage>,
        private val match: (MatchMessage) -> Behavior<UserCrackerMessage>,
        private val noMatch: (NoMatchMessage) -> Behavior<UserCrackerMessage>,
        private val finish: () -> Behavior<UserCrackerMessage>
    ) : Receive<UserCrackerMessage>() {

        override fun receiveSignal(sig: Signal?): Behavior<UserCrackerMessage> = this
        override fun receiveMessage(msg: UserCrackerMessage?): Behavior<UserCrackerMessage> {
            return when(msg) {
                is StartUserCracker -> setupUserCracker.invoke()
                is AllPasswordsTravelled -> finish.invoke()
                is NoMatchMessage -> noMatch.invoke(msg)
                is MatchMessage -> match.invoke(msg)
                else -> { throw IllegalStateException("No Action for Message '$msg' defined.") }
            }
        }
    }
}