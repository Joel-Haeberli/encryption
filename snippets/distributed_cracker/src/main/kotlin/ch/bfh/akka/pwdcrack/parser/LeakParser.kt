package ch.bfh.akka.pwdcrack.parser

import java.nio.file.Files
import java.nio.file.LinkOption
import java.nio.file.Paths
import kotlin.io.path.exists

object LeakParser {

    private const val FILE_LEAK = "leak.txt"

    fun parseLeaks(): List<String> {

        val p = Paths.get(this.javaClass.classLoader.getResource(FILE_LEAK)?.toURI() ?: throw IllegalStateException("Could not load Leaked-Passwords file..."))
        if (p.exists(LinkOption.NOFOLLOW_LINKS)) return Files.readAllLines(p)
        throw IllegalStateException("Could not load key-file $FILE_LEAK.")
    }
}