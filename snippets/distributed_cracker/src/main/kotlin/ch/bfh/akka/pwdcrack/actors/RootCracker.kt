package ch.bfh.akka.pwdcrack.actors

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.Signal
import akka.actor.typed.javadsl.AbstractBehavior
import akka.actor.typed.javadsl.ActorContext
import akka.actor.typed.javadsl.Behaviors
import akka.actor.typed.javadsl.Receive
import ch.bfh.akka.pwdcrack.config.UserConfiguration
import ch.bfh.akka.pwdcrack.config.RootCrackConfiguration
import ch.bfh.akka.pwdcrack.config.User

/**
 * The Root-Cracker manages all actors used in a crack and will not crack passwords himself.
 */
class RootCracker private constructor(
    ctx: ActorContext<RootCrackerMessage>,
    private val config: RootCrackConfiguration = RootCrackConfiguration()
): AbstractBehavior<RootCracker.RootCrackerMessage>(ctx) {

    companion object {

        fun create(config: RootCrackConfiguration) : Behavior<RootCrackerMessage> = Behaviors.setup { ctx -> RootCracker(ctx, config) }
    }

    interface RootCrackerMessage
    class StartRootCracker : RootCrackerMessage
    data class Result(val match: Boolean, val user: User, val pw: String = "") : RootCrackerMessage

    private var numberOfResults = 0
    private val userToUserCrackerActorRef = HashMap<String, ActorRef<UserCracker.UserCrackerMessage>>()

    override fun createReceive() = RootCrackerReceiver(this::setupUserCrackers, this::result)

    /**
     * Creates a UserCracker-Actor for each user in file and calls the setup action on each of UserCracker.
     */
    private fun setupUserCrackers() : RootCracker {

        println("Setting up UserCrackers.")
        for (u in config.users) {
            val userCrackerConfig = UserConfiguration(context.self, u, config.passwords, config.numberOfCrackersPerUser)
            userToUserCrackerActorRef[u.name] = context.spawn(UserCracker.create(userCrackerConfig), "UserCracker-${u.name}")
        }
        userToUserCrackerActorRef.values.forEach { it.tell(UserCracker.StartUserCracker()) }
        println("Setup finished with ${userToUserCrackerActorRef.size} UserCrackers.")
        return this
    }

    private fun result(result: Result) : Behavior<RootCrackerMessage> {
        numberOfResults++
        println("RESULT: user=${result.user.name}, password=${result.pw}")
        if (numberOfResults == userToUserCrackerActorRef.size) {
            println("All passwords conquered. Stopping the cracker.")
            return Behaviors.stopped()
        }
        return this
    }

    class RootCrackerReceiver(
        private val setupUserCrackers: () -> Behavior<RootCrackerMessage>,
        private val result: (Result) -> Behavior<RootCrackerMessage>
    ) : Receive<RootCrackerMessage>() {

        override fun receiveSignal(sig: Signal?): Behavior<RootCrackerMessage> = this
        override fun receiveMessage(msg: RootCrackerMessage?): Behavior<RootCrackerMessage> {
            return when(msg) {
                is StartRootCracker -> setupUserCrackers.invoke()
                is Result -> result.invoke(msg)
                else -> { throw IllegalStateException("No Action for Message '$msg' defined.") }
            }
        }
    }
}