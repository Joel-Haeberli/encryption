package ch.bfh.akka.pwdcrack.actors

import akka.actor.typed.Behavior
import akka.actor.typed.Signal
import akka.actor.typed.javadsl.AbstractBehavior
import akka.actor.typed.javadsl.ActorContext
import akka.actor.typed.javadsl.Behaviors
import akka.actor.typed.javadsl.Receive
import ch.bfh.akka.pwdcrack.config.CrackConfiguration
import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*

class Cracker private constructor(

    ctx: ActorContext<CrackerMessage>,
    private val config: CrackConfiguration

) : AbstractBehavior<Cracker.CrackerMessage>(ctx) {

    companion object {

        fun create(config: CrackConfiguration) : Behavior<CrackerMessage> = Behaviors.setup { ctx -> Cracker(ctx, config) }
    }

    interface CrackerMessage
    data class NextPasswordMessage(val salt: String, val pwIndex: Int) : CrackerMessage

    override fun createReceive() = CrackerReceiver(this::checkHash)

    private fun checkHash(nextPasswordMessage: NextPasswordMessage) : Cracker {
        if (nextPasswordMessage.pwIndex >= config.passwords.size) {
            config.parent.tell(UserCracker.AllPasswordsTravelled())
            return this
        }

        val pw = config.passwords[nextPasswordMessage.pwIndex]
        val concatenated = pw + nextPasswordMessage.salt

        if (config.expected == hash(concatenated))
            config.parent.tell(UserCracker.MatchMessage(pw))
        else
            config.parent.tell(UserCracker.NoMatchMessage(context.self))

       return this
    }

    @Throws(NoSuchAlgorithmException::class)
    fun hash(textToHash: String): String {
        val digest = MessageDigest.getInstance(config.algorithm)
        val byteOfTextToHash = textToHash.toByteArray(StandardCharsets.UTF_8)
        val hashedByteArray = digest.digest(byteOfTextToHash)
        return Base64.getEncoder().encodeToString(hashedByteArray)
    }

    class CrackerReceiver(
        private val nextPassword: (NextPasswordMessage) -> Behavior<CrackerMessage>
    ) : Receive<CrackerMessage>() {

        override fun receiveSignal(sig: Signal?): Behavior<CrackerMessage> = this
        override fun receiveMessage(msg: CrackerMessage?): Behavior<CrackerMessage> {
            return when(msg) {
                is NextPasswordMessage -> nextPassword.invoke(msg)
                else -> { throw IllegalStateException("No Action for Message '$msg' defined.") }
            }
        }

    }
}