package ch.bfh.akka.pwdcrack.config

data class User(val name: String, val salt: String, val hash: String)