package ch.bfh.akka.pwdcrack.parser

import ch.bfh.akka.pwdcrack.config.User
import java.nio.file.Files
import java.nio.file.LinkOption
import java.nio.file.Paths
import kotlin.io.path.exists

object UserParser {

    private const val USER_HASH_FILES = "out.txt"
    private const val SEPARATOR = " "

    fun parseUsers() : List<User> {

        val p = Paths.get(this.javaClass.classLoader.getResource(USER_HASH_FILES)?.toURI() ?: throw IllegalStateException("Could not load User-Hash file..."))
        if (p.exists(LinkOption.NOFOLLOW_LINKS)) return parseUsers(Files.readAllLines(p))
        throw IllegalStateException("Could not load key-file ${USER_HASH_FILES}.")
    }

    private fun parseUsers(userPerLine: List<String>) : List<User> = userPerLine.map(this::parseUser)

    private fun parseUser(userAsLine: String) : User {
        val split = userAsLine.split(SEPARATOR)
        if (split.size != 3) throw IllegalArgumentException("malformed user entry at: $userAsLine")
        return User(split[0], split[1], split[2])
    }
}