package ch.haeberli.encryption.cracks.bruteforce;

import ch.haeberli.encryption.algorithms.Encryption;
import ch.haeberli.encryption.algorithms.Key;

import java.util.HashMap;
import java.util.Map;

/**
 * Chosen-Ciphertext-Attack
 * You can define the key and the cipher yourself.
 * You can feed the attack with two arbitrary strings representing plain- and ciphertext
 */
public abstract class Bruteforce {

    private final Map<Key, String> rainbow = new HashMap<>();

    public Key execute(String plain, String cipher) {
        System.out.println("plain=" + plain + " / ciphertext=" + cipher);
        boolean cracked = false;
        Key crackedKey = null;
        int tries = 0;
        while (!cracked) {
            Key k = next();
            if (rainbow.containsKey(k)) continue;
            else cracked = tryKey(k, plain.toUpperCase(), cipher);
            if (cracked) crackedKey = k;
            tries++;
        }
        System.out.println("Tried " + tries + " keys to crack encrypted message '" + cipher + "'");
        return crackedKey;
    }

    protected abstract Key next();
    protected abstract Encryption createEncryption(Key key);

    protected boolean tryKey(Key k, String plain, String cipher) {
        Encryption enc = createEncryption(k);
        return plain.equals(enc.decrypt(cipher));
    }
}
