package ch.haeberli.encryption.cracks.bruteforce;

import ch.haeberli.encryption.algorithms.Alphabet;
import ch.haeberli.encryption.algorithms.Encryption;
import ch.haeberli.encryption.algorithms.Key;
import ch.haeberli.encryption.algorithms.vigenere.VigenereEncryption;
import ch.haeberli.encryption.algorithms.vigenere.VigenereKey;
import ch.haeberli.encryption.utils.Calculator;

import java.util.HashMap;
import java.util.Map;

public class VigenereBruteforce extends Bruteforce {

    private int keyLength = 1;
    private int currentKeyLengthCombinations = 1;
    private Map<Integer, Key> triedKeysForCurrentLength = new HashMap<>();
    private Alphabet alphabet;

    public VigenereBruteforce(Alphabet alphabet) {
        this.alphabet = alphabet;
    }

    @Override
    protected Key next() {
        if (currentKeyLengthCombinations >= triedKeysForCurrentLength.size()) {
            keyLength++;
            currentKeyLengthCombinations = Calculator.combinations(alphabet.getLength(), keyLength, false);
            triedKeysForCurrentLength = new HashMap<>();
        }
        return new VigenereKey(alphabet, keyLength);
    }

    @Override
    protected Encryption createEncryption(Key key) {
        return new VigenereEncryption(alphabet, (VigenereKey) key);
    }

    @Override
    protected boolean tryKey(Key k, String plain, String cipher) {
        if (triedKeysForCurrentLength.containsValue(k)) return false;
        return super.tryKey(k, plain, cipher);
    }
}
