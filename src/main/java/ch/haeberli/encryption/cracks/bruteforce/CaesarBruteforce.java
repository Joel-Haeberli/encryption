package ch.haeberli.encryption.cracks.bruteforce;

import ch.haeberli.encryption.algorithms.Alphabet;
import ch.haeberli.encryption.algorithms.Encryption;
import ch.haeberli.encryption.algorithms.Key;
import ch.haeberli.encryption.algorithms.caesar.CaesarEncryption;
import ch.haeberli.encryption.algorithms.caesar.CaesarKey;

public class CaesarBruteforce extends Bruteforce {

    private int shift = -1;
    private Alphabet alphabet;

    public CaesarBruteforce(Alphabet alphabet) {
        this.alphabet = alphabet;
    }

    @Override
    protected CaesarKey next() {
        shift++;
        if (shift > alphabet.getLength()) {
            throw new IllegalArgumentException("unable to crack ciphertext!!!");
        }
        System.out.println("Bruteforce: next key=" + shift);
        return new CaesarKey(alphabet, shift);
    }

    @Override
    protected Encryption createEncryption(Key key) {
        return new CaesarEncryption(alphabet, (CaesarKey) key);
    }
}
