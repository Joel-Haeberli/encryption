package ch.haeberli.encryption.utils;

import ch.haeberli.encryption.algorithms.Alphabet;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class AlphabetParser {

    private AlphabetParser() { throw new IllegalStateException("UTILITY"); }

    public static Alphabet parse(String alphabetFileName) {
        try {
            return new Alphabet(parseFile(alphabetFileName));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static List<Character> parseFile(String fileName) throws IOException {
        String all = "";
        try (FileInputStream finp = new FileInputStream(fileName)) {
            all = new String(finp.readAllBytes());
        }
        List<char[]> listOfChars = Arrays.stream(all.split(""))
                .map(String::trim)
                .map(String::toUpperCase)
                .map(String::toCharArray)
                .toList();
        List<Character> alphabet = new ArrayList<>();
        for (char[] listOfChar : listOfChars) {
            for (char c : listOfChar) {
                alphabet.add(c);
            }
        }
        return alphabet;
    }
}
