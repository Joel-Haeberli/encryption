package ch.haeberli.encryption.utils;

public class Calculator {

    private Calculator() { throw new IllegalStateException("UTILITY"); }

    public static int combinations(int possibleValues, int length, boolean allowDuplicates) {
        if (length <= 0 || possibleValues <= 0) throw new IllegalArgumentException("length and possibleValues are not allowed to be zero.");
        if (allowDuplicates) {
            return Double.valueOf(Math.pow(possibleValues, length)).intValue();
        } else {
            if (length > possibleValues) throw new IllegalArgumentException("Not possible to calculate combinations without duplicates, when length bigger than possible values.");
            return factorial(possibleValues) - factorial(possibleValues - length);
        }
    }

    public static int factorial(int i) {
        if (i < 1) throw new IllegalArgumentException("i must be bigger 0.");
        if (i == 1) return i;
        return i * factorial(i - 1);
    }
}
