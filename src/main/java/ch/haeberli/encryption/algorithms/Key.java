package ch.haeberli.encryption.algorithms;

import java.util.Random;

abstract public class Key<K> {

    protected final Random random = new Random();
    protected final Alphabet alphabet;
    protected K key;

    protected Key(Alphabet alphabet) {
        this.alphabet = alphabet;
    }

    protected abstract K generate();
    public abstract int getLength();

    @Override
    public String toString() {
        return key.toString();
    }
}
