package ch.haeberli.encryption.algorithms.caesar;

import ch.haeberli.encryption.algorithms.Alphabet;
import ch.haeberli.encryption.algorithms.Key;

public class CaesarKey extends Key<Integer> {

    private int shift = 3;

    public CaesarKey(Alphabet alphabet, int shift) {
        this(alphabet);
        this.shift = shift;
    }

    protected CaesarKey(Alphabet alphabet) {
        super(alphabet);
    }

    public int getShift() {
        return shift;
    }

    @Override
    protected Integer generate() {
        return getShift();
    }

    @Override
    public int getLength() {
        return 0;
    }
}
