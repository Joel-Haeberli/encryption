package ch.haeberli.encryption.algorithms.caesar;

import ch.haeberli.encryption.algorithms.Alphabet;
import ch.haeberli.encryption.algorithms.Encryption;

import java.util.function.Function;

public class CaesarEncryption extends Encryption {

    private CaesarKey key;
    private final Function<Integer, Integer> ENC_OPERATOR = (i) -> i + key.getShift();
    private final Function<Integer, Integer> DEC_OPERATOR = (i) -> i - key.getShift();

    public CaesarEncryption(Alphabet alphabet) {
        this(alphabet, new CaesarKey(alphabet, 3));
    }

    public CaesarEncryption(Alphabet alphabet, CaesarKey key) {
        super(alphabet, null);
        this.key = key;
    }

    @Override
    public String encrypt(String plain) {
        return transform(plain, ENC_OPERATOR);
    }

    @Override
    public String decrypt(String ciphertext) {
        return transform(ciphertext, DEC_OPERATOR);
    }

    protected String transform(String content, Function<Integer, Integer> transformOperation) {
        if (content == null || content.length() < 1) throw new IllegalArgumentException("please supply content.");
        char[] origin = content.toCharArray();
        char[] result = new char[origin.length];
        for (int i = 0; i < origin.length; i++) {
            int indexOfOrigin = alphabet.indexOf(origin[i]);
            int indexOfResult = transformOperation.apply(indexOfOrigin) % (alphabet.getLength() - 1);
            result[i] = alphabet.elementAt(indexOfResult < 0 ? indexOfResult + (alphabet.getLength() - 1) : indexOfResult);
        }
        return new String(result);
    }
}
