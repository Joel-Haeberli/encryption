package ch.haeberli.encryption.algorithms;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

public class Alphabet {

    private final Character[] characters;

    public Alphabet(List<Character> characters) {
        this.characters = new Character[characters.size()];
        characters.toArray(this.characters);
    }

    public int getLength() {
        return characters.length;
    }

    public char elementAt(int i) {
        if (i < 0 || i > getLength() - 1) throw new IndexOutOfBoundsException("Illegal index " + i + " given.");
        return characters[i];
    }

    public int indexOf(char c) {
        for (int i = 0; i < characters.length; i++) {
            if (characters[i] == c) {
                return i;
            }
        }
        throw new NoSuchElementException("No character '" + c + "' in alphabet");
    }

    @Override
    public String toString() {
        return Arrays.toString(characters);
    }
}
