package ch.haeberli.encryption.algorithms;

public abstract class Encryption {

    protected final Alphabet alphabet;
    protected final Key key;

    protected Encryption(Alphabet alphabet, Key key) {
        this.alphabet = alphabet;
        this.key = key;
    }

    public abstract String encrypt(String plain);
    public abstract String decrypt(String ciphertext);
}
