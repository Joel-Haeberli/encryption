package ch.haeberli.encryption.algorithms.vigenere;

import ch.haeberli.encryption.algorithms.Alphabet;
import ch.haeberli.encryption.algorithms.Key;
import ch.haeberli.encryption.algorithms.Mode;

import java.util.Arrays;

public class VigenereKey extends Key<Alphabet> {

    private int length = -1;

    public VigenereKey(Alphabet alphabet) {
        super(alphabet);
        this.key = generate();
        System.out.println("Generated key: " + key);
    }

    public VigenereKey(Alphabet alphabet, Alphabet key) {
        super(alphabet);
        this.key = key;
    }

    public VigenereKey(Alphabet alphabet, Integer length) {
        super(alphabet);
        this.length = length;
        this.key = generate();
        System.out.println("Generated key: " + key.toString());
    }

    @Override
    protected Alphabet generate() {
        return generate(length < 0 ? random.nextInt(1, alphabet.getLength()) : length);
    }

    protected Alphabet generate(int len) {
        Character[] key = new Character[len];
        for (int i = 0; i < len; i++) {
            int randomCharIndex = random.nextInt(0, alphabet.getLength() - 1);
            key[i] = alphabet.elementAt(randomCharIndex);
        }
        return new Alphabet(Arrays.asList(key));
    }

    @Override
    public int getLength() {
        return key.getLength();
    }

    public char transform(char c, int keyIndex, Mode mode) {
        int indexOfChar = alphabet.indexOf(c);

        int encryptTransformIndex = (indexOfChar + delta(keyIndex));

        int decryptTransformIndex = (indexOfChar - delta(keyIndex));
        decryptTransformIndex = decryptTransformIndex < 0 ?
                decryptTransformIndex + alphabet.getLength() - 1 :
                decryptTransformIndex;

        return alphabet.elementAt(
                Mode.ENCRYPT.equals(mode) ?
                        encryptTransformIndex % (alphabet.getLength() - 1):
                        decryptTransformIndex % (alphabet.getLength() - 1)
        );
    }

    public int delta(int keyIndex) {
        return alphabet.indexOf(key.elementAt(keyIndex));
    }
}
