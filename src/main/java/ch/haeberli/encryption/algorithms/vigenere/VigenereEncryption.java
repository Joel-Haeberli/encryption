package ch.haeberli.encryption.algorithms.vigenere;

import ch.haeberli.encryption.algorithms.Alphabet;
import ch.haeberli.encryption.algorithms.Encryption;
import ch.haeberli.encryption.algorithms.Mode;

public class VigenereEncryption extends Encryption {

    public VigenereEncryption(Alphabet alphabet, VigenereKey key) {
        super(alphabet, key);
    }

    @Override
    public String encrypt(String plain) {
        return transform(plain, Mode.ENCRYPT);
    }

    @Override
    public String decrypt(String ciphertext) {
        return transform(ciphertext, Mode.DECRYPT);
    }

    protected String transform(String content, Mode mode) {
        if (content == null || content.length() < 1) throw new IllegalArgumentException("please supply content.");
        char[] origin = content.toCharArray();
        char[] result = new char[origin.length];
        int keyIndex = 0;
        for (int i = 0; i < origin.length; i++) {
            result[i] = ((VigenereKey) key).transform(origin[i], keyIndex, mode);
            keyIndex = keyIndex < key.getLength() - 1 ? keyIndex + 1 : 0;
        }
        return new String(result);
    }
}
