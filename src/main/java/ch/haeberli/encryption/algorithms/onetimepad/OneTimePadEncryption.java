package ch.haeberli.encryption.algorithms.onetimepad;

import ch.haeberli.encryption.algorithms.Alphabet;
import ch.haeberli.encryption.algorithms.vigenere.VigenereEncryption;

public class OneTimePadEncryption extends VigenereEncryption {

    public OneTimePadEncryption(Alphabet alphabet, OneTimePadKey key) {
        super(alphabet, key);
    }
}
