package ch.haeberli.encryption.algorithms.onetimepad;

import ch.haeberli.encryption.algorithms.Alphabet;
import ch.haeberli.encryption.algorithms.vigenere.VigenereKey;

public class OneTimePadKey extends VigenereKey {

    private final int msgLen;

    public OneTimePadKey(Alphabet alphabet, int msgLen) {
        super(alphabet);
        this.msgLen = msgLen;
        this.key = generate();
        System.out.println("Generated key: " + key);
    }

    @Override
    protected Alphabet generate() {
        return generate(msgLen);
    }

    @Override
    public int getLength() {
        return 0;
    }
}
