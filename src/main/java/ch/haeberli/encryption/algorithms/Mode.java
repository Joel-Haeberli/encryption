package ch.haeberli.encryption.algorithms;

public enum Mode {
    ENCRYPT,
    DECRYPT
}
