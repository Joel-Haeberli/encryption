package ch.haeberli.encryption.algorithms;

public class Cipher {

    private final Encryption encryption;
    private Mode mode = Mode.ENCRYPT;

    public Cipher(Encryption enc) {
        this.encryption = enc;
    }

    public String process(String content) {
        return mode == Mode.ENCRYPT ?
                encryption.encrypt(normalize(content)) :
                encryption.decrypt(normalize(content));
    }

    public void switchMode() {
        mode = mode == Mode.ENCRYPT ? Mode.DECRYPT : Mode.ENCRYPT;
    }

    private String normalize(String input) {
        if (input == null || input.length() < 1) throw new IllegalArgumentException("please supply content.");
        return input
                .replace(" ", "")
                .toUpperCase();
    }
}
