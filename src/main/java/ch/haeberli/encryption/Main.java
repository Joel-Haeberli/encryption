package ch.haeberli.encryption;

import ch.haeberli.encryption.algorithms.Alphabet;
import ch.haeberli.encryption.algorithms.Cipher;
import ch.haeberli.encryption.algorithms.caesar.CaesarEncryption;
import ch.haeberli.encryption.algorithms.caesar.CaesarKey;
import ch.haeberli.encryption.algorithms.onetimepad.OneTimePadEncryption;
import ch.haeberli.encryption.algorithms.onetimepad.OneTimePadKey;
import ch.haeberli.encryption.algorithms.vigenere.VigenereEncryption;
import ch.haeberli.encryption.algorithms.vigenere.VigenereKey;
import ch.haeberli.encryption.cracks.bruteforce.CaesarBruteforce;
import ch.haeberli.encryption.cracks.bruteforce.VigenereBruteforce;
import ch.haeberli.encryption.utils.AlphabetParser;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Supply message to encrypt and decrypt (one argument)");
            return;
        }
        String content = args[0];
        Alphabet latin = AlphabetParser.parse("src/main/resources/latin.txt");
        Alphabet binary = AlphabetParser.parse("src/main/resources/binary.txt");

        caesar(content, latin);
        caesar("0101010101", binary);
        vigenere(content, latin);
        onepad(content, latin);

        bruteforceCaesar(content, "WKLVLVDORQJVHQWHQFHAKLFKALOOEHHQFUCSWHG", latin);
        bruteforceCaesar(content, "SGHRHRYKNMFRDMSDMBDVGHBGVHKKADDMBQXOSDC", latin);
        bruteforceCaesar("ARENA", "EVIRE", latin);
        try { bruteforceCaesar("RIVER", "EVIRE", latin); } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        bruteforceVigenere("ARENA", "EVIRE", latin);
        bruteforceVigenere("RIVER", "EVIRE", latin);

        // ATTACKATDAWN
        System.out.println("\n\n");
        Cipher vigenere = new Cipher(new VigenereEncryption(latin, new VigenereKey(latin, new Alphabet(List.of('D', 'U', 'H')))));
        String encrypted = vigenere.process("ATTACKATDAWN");
        vigenere.switchMode();
        String decrypted = vigenere.process(encrypted);
        System.out.println("VIGENERE ENCRYPTION:\n-ENC-> " + encrypted + "\n<-DEC- " + decrypted + "\n\n");
    }

    private static void caesar(String content, Alphabet alphabet) {
        Cipher caesar = new Cipher(new CaesarEncryption(alphabet));
        String encrypted = caesar.process(content);
        caesar.switchMode();
        String decrypted = caesar.process(encrypted);
        System.out.println("CAESAR ENCRYPTION (shift=3):\n-ENC-> " + encrypted + "\n<-DEC- " + decrypted + "\n\n");

        caesar.switchMode();
        caesar = new Cipher(new CaesarEncryption(alphabet, new CaesarKey(alphabet, 1)));
        encrypted = caesar.process(content);
        caesar.switchMode();
        decrypted = caesar.process(encrypted);
        System.out.println("CAESAR ENCRYPTION (shift=1):\n-ENC-> " + encrypted + "\n<-DEC- " + decrypted + "\n\n");
    }

    private static void bruteforceCaesar(String plain, String ciphertext, Alphabet alphabet) {
        CaesarBruteforce caesarBruteforce = new CaesarBruteforce(alphabet);
        System.out.println("\nBruteforce Caesar");
        CaesarKey key = (CaesarKey) caesarBruteforce.execute(plain, ciphertext);
        System.out.println("CRACKED! Key is shifting " + key.getShift());
    }

    private static void vigenere(String content, Alphabet alphabet) {
        Cipher vigenere = new Cipher(new VigenereEncryption(alphabet, new VigenereKey(alphabet)));
        String encrypted = vigenere.process(content);
        vigenere.switchMode();
        String decrypted = vigenere.process(encrypted);
        System.out.println("VIGENERE ENCRYPTION:\n-ENC-> " + encrypted + "\n<-DEC- " + decrypted + "\n\n");
    }

    private static void bruteforceVigenere(String plain, String ciphertext, Alphabet alphabet) {
        VigenereBruteforce vigenereBruteforce = new VigenereBruteforce(alphabet);
        System.out.println("\nBruteforce Caesar");
        VigenereKey key = (VigenereKey) vigenereBruteforce.execute(plain, ciphertext);
        System.out.println("CRACKED! Key is shifting " + key);
    }

    private static void onepad(String content, Alphabet alphabet) {
        Cipher onepad = new Cipher(new OneTimePadEncryption(alphabet, new OneTimePadKey(alphabet, content.length())));
        String encrypted = onepad.process(content);
        onepad.switchMode();
        String decrypted = onepad.process(encrypted);
        System.out.println("ONEPAD ENCRYPTION:\n" + encrypted + "\n" + decrypted + "\n\n");
    }
}